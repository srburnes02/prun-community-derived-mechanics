---
title: "HQ"
---

## Upgrade Costs

| From | To  | Part Costs                                                     |
|------|-----|----------------------------------------------------------------|
| 2    | 3   | 12 BBH, 8 BDE, 8 BSE, 4 BTA, 24 MCG, 8 TRU                     |
| 3    | 4   | 12 BBH, 8 BDE, 8 BSE, 4 BTA, 24 MCG, 8 TRU                     |
| 4    | 5   | 24 BBH, 16 BDE, 16 BSE, 8 BTA, 48 MCG, 16 TRU                  |
| 5    | 6   | 48 BBH, 32 BDE, 32 BSE, 16 BTA, 96 MCG, 32 TRU                 |
| 6    | 7   | 96 BBH, 64 BDE, 64 BSE, 32 BTA, 192 MCG, 64 TRU                |
| 7    | 8   | 14 LBH, 10 LDE, 10 LSE, 6 LTA, 40 MCG, 15 OFF, 10 TRU          |
| 8    | 9   | 25 LBH, 18 LDE, 18 LSE, 11 LTA, 70 MCG, 26 OFF, 18 TRU         |
| 9    | 10  | 43 LBH, 31 LDE, 31 LSE, 18 LTA, 123 MCG, 46 OFF, 31 TRU        |
| 10   | 11  | 75 LBH, 54 LDE, 54 LSE, 32 LTA, 214 MCG, 80 OFF, 54 TRU        |
| 11   | 12  | 131 LBH, 94 LDE, 94 LSE, 56 LTA, 375 MCG, 141 OFF, 94 TRU      |
| **Prediction**                                                              |
| 12   | 13  | 230 LBH, 164 LDE, 98 LTA, 657 MCG, 246 OFF, 164 TRU            |

The prediction is based on the follow formula:

`BaseCost * 1.75^n`, where n = "prefab tier-count" and n is 0 starting at the switch from 7 -> 8 (when L-fabs started). The formula will most likely change once it switches to R-tier Fab requirements.

## HQ Bonus

The multiplier to the base efficiency bonus provided by the HQ can be given by:

`Multiplier = -2 * (UsedPermits / TotalPermits) + 3`